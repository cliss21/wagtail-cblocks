��          �      |      �  D   �     6     B     I     Q     X     `     i     w     �     �     �     �     �     �  	   �     �     �     �     �     �    �  F        V     e     l     u     |     �     �     �     �     �     �     �     �     �  
   �     �     �                                                 	         
                                                      An anchor in the current page, for example: <code>#target-id</code>. Anchor link Button Caption Center Columns Document External link Horizontal alignment Image Left Level Level %(level)d heading Link Page Paragraph Right Style Text This field is required. Title Project-Id-Version: wagtail-cblocks
Report-Msgid-Bugs-To: 
PO-Revision-Date: 2024-06-04 16:46+0200
Last-Translator: 
Language-Team: 
Language: fr
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=(n > 1);
 Une ancre dans la page courante, par exemple : <code>#id-cible</code>. Lien d'ancrage Bouton Légende Milieu Colonnes Document Lien externe Alignement horizontal Image Gauche Niveau Titre de niveau %(level)d Lien Page Paragraphe Droite Style Texte Ce champ est requis. Titre 